package com.hkrt.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.hkrt.service.HelloService;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Surpass
 * @Package com.hkrt.service.impl
 * @Description: ${todo}
 * @date 2020/10/27 15:52
 */
@Service   //发布服务必须使用Dubbo提供的service注解
@Transactional
public class HelloServiceImpl implements HelloService {

    @Override
    public String sayHello(String name) {
        return "hello"+name;
    }
}
